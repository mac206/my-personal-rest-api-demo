from typing import Union, List, Dict

from fastapi import FastAPI

import os
import datetime

app = FastAPI()

submissions = []


@app.get("/")
def read_root():
    return {"message": "Hello World"}


@app.get("/uptime")
def return_uptime():
    awake_since = os.popen('uptime -p').read()
    right_now = datetime.datetime.now()
    return {"server_uptime": awake_since,
            "current_timestamp": right_now}


# @app.post("/polypeptide/{peptide_string}")
# def read_peptide(peptide: Union[str, None] = None):
#     return {"peptide": peptide}

@app.post("/polypeptide/{peptide_string}")
def create_peptide(peptide_string: str):
    # Create a new submission with the current timestamp
    submission = {
        "timestamp": datetime.datetime.now().isoformat(),
        "peptide": peptide_string
    }
    submissions.append(submission)
    return {"message": "Submission received"}

@app.get("/polypeptide", response_model=List[Dict[str, Union[str, datetime.datetime]]])
def read_polypeptides():
    if not submissions:
        raise HTTPException(status_code=404, detail="No submissions found")
    return submissions

